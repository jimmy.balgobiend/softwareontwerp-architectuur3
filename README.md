# Softwareontwerp architectuur3

Softwareontwerp & - architectuur 3
Afrash Ramjit
Jimmy Balgobiend

## State pattern
https://drive.google.com/file/d/11RGZKmljt9oIf5WSiqYStQJrGsboUGaH/view?usp=sharing

## Obervable & adapter pattern
https://drive.google.com/file/d/1zcJ7hvVbmbsDFS1Eo76UFMCeF8_Fln1R/view?usp=sharing

## Composite pattern
https://drive.google.com/file/d/18XVIciiW0xXrcpgaLzlpEWWfUYgxk0LK/view?usp=sharing
