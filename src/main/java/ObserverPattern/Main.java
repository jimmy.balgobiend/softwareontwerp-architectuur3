package ObserverPattern;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        // write your code here
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();

        // Create movies
        movies.add(new Movie("Avengers"));
        movies.add(new Movie("Spiderman"));

        // Create screenings
        movies.get(0).addScreening(new MovieScreening(movies.get(0), LocalDateTime.now(), 11.50));
        movies.get(1).addScreening(new MovieScreening(movies.get(1), LocalDateTime.now().plusDays(2), 14.50));

        // Create orders
        // 1st
        orders.add(new Order(1, false));
        orders.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 155));
        orders.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 156));
        // 2e
        orders.add(new Order(2, false));
        orders.get(1).addSeatReservation(new MovieTicket(movies.get(1).getMovieScreenings().get(0), true, 12, 75));
        // 3e
        orders.add(new Order(3, true));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 100));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 101));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 102));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 103));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 104));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 105));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 106));

        ArrayList<Movie> movies1 = new ArrayList<>();
        movies1.add(new Movie("Avengers"));
        List<Order> orders1 = new ArrayList<>();
        orders1.add(new Order(1, false));
        orders1.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 155));
        orders1.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 156));
        orders1.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 155));
        orders1.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 156));

        var order = new Order(5, true);
        order.selectShow();
        order.getState().selectShow();

        order.getState().submit();
//        order.cancel();

//        System.out.println(orders1.get(0).calculatePrice());
//
//        for(int i =0; i < orders.size(); i++){
//            System.out.println(orders.get(i).toString());
//        }
//
//        System.out.println(orders.get(2).calculatePrice());
//
//        // Results
//        orders.get(0).export(TicketExportFormat.JSON);
//        orders.get(2).export(TicketExportFormat.PLAINTEXT);


    }
}
