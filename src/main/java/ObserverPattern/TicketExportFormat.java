package ObserverPattern;
public enum TicketExportFormat {
    PLAINTEXT,
    JSON
}
