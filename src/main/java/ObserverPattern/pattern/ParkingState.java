package ObserverPattern.pattern;

import ObserverPattern.MovieTicket;
import ObserverPattern.Order;

import java.util.ArrayList;

public class ParkingState implements State {

    Order order;

    public ParkingState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow() {
        System.out.println("Not available");
    }

    @Override
    public void selectTickets(ArrayList<MovieTicket> movieTickets) {
        System.out.println("Not available");
    }

    @Override
    public void parking(boolean containsParking) {
        System.out.println("Parking is " + containsParking);
    }

    @Override
    public void memoryNotPaid() {
        System.out.println("Not available");
    }

    @Override
    public void cancel() {
        System.out.println("Not available");
    }

    @Override
    public void submit() {
        System.out.println("Not available");
    }
}
