package ObserverPattern.pattern;

import ObserverPattern.MovieTicket;
import ObserverPattern.Order;

import java.util.ArrayList;

public class SelectShowState implements State {

    Order order;

    public SelectShowState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow() {
        //var movie = order.getMovies().get(0).getMovieScreenings().get(0).getMovie();
        System.out.println("show selected");
//        order.setState(order.getSelectTicketsState());
    }

    @Override
    public void selectTickets(ArrayList<MovieTicket> movieTickets) {
        System.out.println("No tickets selected");
    }

    @Override
    public void parking(boolean containsParking) {
        System.out.println("No parking tickets");
    }

    @Override
    public void memoryNotPaid() {
        System.out.println("No order is done");
    }

    @Override
    public void cancel() {
        System.out.println("You need to order first");
    }

    @Override
    public void submit() {
        System.out.println("You need to order first");
    }
}
