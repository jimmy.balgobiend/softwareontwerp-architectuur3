package ObserverPattern.pattern;

import ObserverPattern.MovieTicket;
import ObserverPattern.Order;

import java.util.ArrayList;

public class SelectTicketsState implements State {

    Order order;

    public SelectTicketsState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow() {
        System.out.println("Not available");
    }

    @Override
    public void selectTickets(ArrayList<MovieTicket> movieTickets) {
        System.out.println("Print selected ticket");
    }

    @Override
    public void parking(boolean containsParking) {
        System.out.println("Not available");
    }

    @Override
    public void memoryNotPaid() {
        System.out.println("Not available");
    }

    @Override
    public void cancel() {
        System.out.println("Not available");
    }

    @Override
    public void submit() {
        System.out.println("Not available");
    }
}
