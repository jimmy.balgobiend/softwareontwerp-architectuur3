package ObserverPattern.pattern;

import ObserverPattern.MovieTicket;

import java.util.ArrayList;

public interface State {
    void selectShow();
    void selectTickets(ArrayList<MovieTicket> movieTickets);
    void parking(boolean containsParking);
    void memoryNotPaid();
    void cancel();
    void submit();

}
