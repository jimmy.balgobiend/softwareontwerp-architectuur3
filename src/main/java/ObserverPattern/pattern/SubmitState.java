package ObserverPattern.pattern;

import ObserverPattern.MovieTicket;
import ObserverPattern.Order;

import java.util.ArrayList;

public class SubmitState implements State {
    Order order;

    public SubmitState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow() {
        System.out.println("Correct");
    }

    @Override
    public void selectTickets(ArrayList<MovieTicket> movieTickets) {
        System.out.println("Tickets are selected");
    }

    @Override
    public void parking(boolean containsParking) {
        System.out.println("Parking is selected");
    }

    @Override
    public void memoryNotPaid() {
        System.out.println("is paid");
    }

    @Override
    public void cancel() {
        System.out.println("Cannot cancel");
    }

    @Override
    public void submit() {
        System.out.println("submitted succesfull");
    }
}
