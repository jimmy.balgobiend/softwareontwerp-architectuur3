package StatePattern;

import java.util.ArrayList;

public class Movie {
    private String title;

    private ArrayList<MovieScreening> movieScreenings = new ArrayList<MovieScreening>();

    public Movie(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void addScreening(MovieScreening screening){
        movieScreenings.add(screening);
    }

    public ArrayList<MovieScreening> getMovieScreenings() {
        return movieScreenings;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                '}';
    }
}
