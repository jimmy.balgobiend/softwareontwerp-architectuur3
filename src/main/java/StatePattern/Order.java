package StatePattern;

import StatePattern.pattern.*;

import java.io.FileWriter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private final int orderNr;
    private final boolean isStudentOrder;
    private final ArrayList<MovieTicket> tickets = new ArrayList<>();
    private ArrayList<Movie> movies = new ArrayList<>();

    State hasNoOrderState;
    State hasOrderState;
    State submittedState;
    State finishedOrderState;
    State cancelledOrderState;

    State state;

    public Order(int orderNr, boolean isStudentOrder) {
        movies.add(new Movie("avenge"));
        movies.add(new Movie("Nighthouse"));
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;

        this.hasNoOrderState = new NoOrderState(this);
        this.hasOrderState = new HasOrderState(this);
        this.submittedState = new SubmittedOrderState(this);
        this.finishedOrderState = new FinishedOrderState(this);
        this.cancelledOrderState = new CancelledOrderState(this);

        this.state = hasNoOrderState;
    }

    public void create(){
        this.state.create();
    };



    public State getCancelledOrderState() {
        return cancelledOrderState;
    }



    public void pay(){
        this.state.pay();
    }

    public void cancel(){
        this.state.cancel();
    }

    public void submit(){
        this.state.submit();
    }

    public void change(){
        this.state.change();
    };

    public void setState(State state) {
        this.state = state;
    }

    public State getHasNoOrderState() {
        return hasNoOrderState;
    }

    public State getHasOrderState() {
        return hasOrderState;
    }

    public State getSubmittedState() {
        return submittedState;
    }


    public State getFinishedOrderState() {
        return finishedOrderState;
    }


    public State getState() {
        return state;
    }

    public boolean isStudentOrder() {
        return isStudentOrder;
    }

    public List<MovieTicket> getTickets() {
        return tickets;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket) {
        this.tickets.add(ticket);
    }

    public ArrayList<Movie> getMovies() {
        return movies;
    }

    //Person: student, no student
    //Premium: not premium, premium
    //second ticket: difference in student or no student
    //10%discount 6 || 6> tickets for non students
    public double calculatePrice() {
        var totalPrice = 0.00;

        for (int i = 0; i < this.tickets.size(); i++) {
            var dateTicket = this.tickets.get(i).getMovieScreening().getDateAndTime();
            //is student
            if (this.isStudentOrder) {
                //second ticket is free for students(every day)
                if (i % 2 == 0) {
                    totalPrice += this.tickets.get(i).getPrice();
                } else {
                    totalPrice += 0;
                }
                //premium tickets for students = 2$ more expensive then standard price per seat.
                if (this.tickets.get(i).isPremiumTicket()) {
                    totalPrice += this.tickets.get(i).getPrice() + 2;
                }
            } else {
                //non students
                //second ticket is free for everyone on ma/di/wo/do
                if (i % 2 == 0) {
                    totalPrice += this.tickets.get(i).getPrice();
                } else {

                    if (dateTicket.getDayOfWeek() == DayOfWeek.MONDAY || dateTicket.getDayOfWeek() == DayOfWeek.TUESDAY || dateTicket.getDayOfWeek() == DayOfWeek.WEDNESDAY || dateTicket.getDayOfWeek() == DayOfWeek.THURSDAY) {
                        totalPrice += 0;
                    } else {
                        //normal price if on FRI/SAT/SUN
                        totalPrice = totalPrice + this.tickets.get(i).getPrice();
                    }
                }
                //premium tickets for non students = 3$ more expensive then standard price per seat.
                if (this.tickets.get(i).isPremiumTicket()) {
                    totalPrice += this.tickets.get(i).getPrice() + 3;
                }
            }
        }
        //weekend as non student = fill price, unless order = 6 tickets or 6> then 10% discount.
        if (this.tickets.size() >= 6) {
            totalPrice = totalPrice * 0.9;
        }
        return totalPrice;
    }

    public void export(TicketExportFormat exportFormat) throws IOException {
        if (exportFormat == TicketExportFormat.JSON) {
            try( FileWriter writer = new FileWriter("testfile.json")){
                for (int i = 0; i < this.tickets.size(); i++) {
                    writer.write(this.tickets.get(i).toString());
                    writer.write(this.tickets.get(i).getMovieScreening().toString());
                    writer.write(String.valueOf(this.calculatePrice()));
                }
            }catch(Exception e){
                System.err.println(e);
            }
        }
        if (exportFormat == TicketExportFormat.PLAINTEXT) {
            try( FileWriter writer = new FileWriter("testfile.txt")){
                for (int i = 0; i < this.tickets.size(); i++) {
                    writer.write(this.tickets.get(i).toString());
                    writer.write(this.tickets.get(i).getMovieScreening().toString());
                    writer.write(Double.toString(this.calculatePrice()));
                }
            }catch(Exception e){
                System.err.println(e);
            }
        }
    }
}
