package StatePattern;
public enum TicketExportFormat {
    PLAINTEXT,
    JSON
}
