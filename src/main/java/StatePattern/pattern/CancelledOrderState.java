package StatePattern.pattern;

import StatePattern.Order;

public class CancelledOrderState implements State {

    Order order;

    public CancelledOrderState(Order order) {
        this.order = order;
    }

    @Override
    public void create() {
        System.out.println("Order is cancelled");

    }

    @Override
    public void pay() {

        System.out.println("Order is cancelled");
    }

    @Override
    public void change() {
        System.out.println("Order is cancelled");
    }

    @Override
    public void submit() {
        System.out.println("Order is cancelled");
    }

    @Override
    public void cancel() {
        System.out.println("Order is already cancelled");
    }
}
