package StatePattern.pattern;

import StatePattern.MovieTicket;
import StatePattern.Order;

import java.util.ArrayList;

public class FinishedOrderState implements State{

    Order order;

    public FinishedOrderState(Order order) {
        this.order = order;
    }

    @Override
    public void create() {
        System.out.println("Order is already created");
    }


    @Override
    public void pay() {
        System.out.println("Order is already paid");
    }

    @Override
    public void change() {
        System.out.println("Cannot changed order when paid");
    }

    @Override
    public void submit() {
        System.out.println("Order is already paid");
    }

    @Override
    public void cancel() {
        System.out.println("Cannot cancel when paid");
    }
}
