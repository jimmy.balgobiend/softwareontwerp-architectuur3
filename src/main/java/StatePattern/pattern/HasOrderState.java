package StatePattern.pattern;
import StatePattern.Order;

public class HasOrderState implements State{

    Order order;

    public HasOrderState(Order order) {
        this.order = order;
    }

    @Override
    public void create() {
        System.out.println("Cannot create two orders at the same time");
    }

    @Override
    public void pay() {
        System.out.println("You need to submit the order first");
    }

    @Override
    public void change() {
        System.out.println("Order is changed");
    }

    @Override
    public void submit() {
        System.out.println("Order is submitted");
        order.setState(order.getSubmittedState());
    }

    @Override
    public void cancel() {
        System.out.println("Order is cancelled");
        order.setState(order.getCancelledOrderState());
    }
}
