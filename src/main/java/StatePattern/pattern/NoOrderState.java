package StatePattern.pattern;

import StatePattern.Order;

public class NoOrderState implements State {

    Order order;

    public NoOrderState(Order order) {
        this.order = order;
    }

    @Override
    public void create() {
        System.out.println("Movie and ticket is selected and an order is created");
        order.setState(order.getHasOrderState());
    }

    @Override
    public void pay() {
        System.out.println("You need to submit an order first");
    }

    @Override
    public void change() {
        System.out.println("You need to create an order first");
    }

    @Override
    public void submit() {
        System.out.println("You need to create an order first");
    }

    @Override
    public void cancel() {
        System.out.println("You need to create an order first");
    }
}
