package StatePattern.pattern;

import StatePattern.Movie;
import StatePattern.MovieTicket;

import java.util.ArrayList;

public interface State {
    void create();
    void pay();
    void change();
    void submit();
    void cancel();

}
