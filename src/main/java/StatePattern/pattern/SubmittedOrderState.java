package StatePattern.pattern;
import StatePattern.Order;


public class SubmittedOrderState implements State{

    Order order;

    public SubmittedOrderState(Order order) {
        this.order = order;
    }

    @Override
    public void create() {
        System.out.println("Order already created");
    }

    @Override
    public void pay() {
        System.out.println("Order is paid");
        order.setState(order.getFinishedOrderState());
    }

    @Override
    public void change() {
        System.out.println("Order is changed");
    }

    @Override
    public void submit() {
        System.out.println("Order is already submitted");
    }

    @Override
    public void cancel() {
        System.out.println("Order is cancelled");
        order.setState(order.getCancelledOrderState());
    }
}
