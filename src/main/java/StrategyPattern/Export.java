package StrategyPattern;

import java.io.FileWriter;
import java.util.ArrayList;

public interface Export {
    void exportTickets(ArrayList<MovieTicket> tickets, Order order);
}
