package StrategyPattern;

import java.io.FileWriter;
import java.util.ArrayList;

public class ExportToPlainText implements Export {

    @Override
    public void exportTickets(ArrayList<MovieTicket> tickets, Order order) {
        try (FileWriter writer = new FileWriter("testfile.txt")) {
            for (int i = 0; i < tickets.size(); i++) {
                writer.write(tickets.get(i).toString());
                writer.write(tickets.get(i).getMovieScreening().toString());
                writer.write(Double.toString(order.calculatePrice()));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}