package StrategyPattern;

import java.time.LocalDateTime;

public class MovieScreening {
    private LocalDateTime dateAndTime;
    private double pricePerSeat;
    private Movie movie;

    public MovieScreening(Movie movie, LocalDateTime dateAndTime, double pricePerSeat) {
        this.movie = movie;
        this.dateAndTime = dateAndTime;
        this.pricePerSeat = pricePerSeat;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    public double getPricePerSeat(){
        return this.pricePerSeat;
    }

    @Override
    public String toString() {
        return "MovieScreening{" +
                "dateAndTime=" + dateAndTime +
                ", pricePerSeat=" + pricePerSeat +
                ", movie=" + movie +
                '}';
    }
}