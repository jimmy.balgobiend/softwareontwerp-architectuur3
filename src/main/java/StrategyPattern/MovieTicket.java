package StrategyPattern;

public class MovieTicket {
    private int rowNr;
    private int seatNr;
    public TicketType ticketType;
    private MovieScreening movieScreening;

    public MovieTicket(MovieScreening movieScreening, int seatRow, int seatNr) {
        this.rowNr = seatRow;
        this.seatNr = seatNr;
        this.movieScreening = movieScreening;
    }

    public boolean isPremiumTicket(){
        return ticketType.isPremiumTicket();
    }

    public void setTicketType(TicketType newTicketType){
        ticketType = newTicketType;
    }

    public MovieScreening getMovieScreening() {
        return movieScreening;
    }

    public double getPrice(){
        return this.movieScreening.getPricePerSeat();
    }

    @Override
    public String toString() {
        return "MovieTicket{" +
                "rowNr=" + rowNr +
                ", seatNr=" + seatNr +
                ", movieScreening=" + movieScreening +
                '}';
    }
}
