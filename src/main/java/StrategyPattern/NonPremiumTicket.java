package StrategyPattern;

public class NonPremiumTicket extends MovieTicket {
    public NonPremiumTicket(MovieScreening movieScreening, int seatRow, int seatNr) {
        super(movieScreening, seatRow, seatNr);
        ticketType = new isNotPremium();
    }
}
