package StrategyPattern;

public class NonStudentWeekDays extends Order {
    public NonStudentWeekDays(int orderNr) {
        super(orderNr);
        orderType = new NonStudentWeekDaysOrderType();
    }
}