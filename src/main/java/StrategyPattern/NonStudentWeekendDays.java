package StrategyPattern;

public class NonStudentWeekendDays extends Order {
    public NonStudentWeekendDays(int orderNr) {
        super(orderNr);
        orderType = new NonStudentWeekendEndDaysOrderType();
    }
}
