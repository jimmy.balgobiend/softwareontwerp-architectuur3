package StrategyPattern;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

abstract public class Order {
    private final int orderNr;
    public OrderType orderType;
    private final ArrayList<MovieTicket> tickets = new ArrayList<>();

    public Order(int orderNr) {
        this.orderNr = orderNr;
    }

    public List<MovieTicket> getTickets() {
        return tickets;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket) {
        this.tickets.add(ticket);
    }

    public boolean isStudentOrder(){
        return orderType.isStudentOrder();
    }

    public boolean isWeekend(){
        return orderType.isWeekend();
    }

    public void setOrderType(OrderType newOrderType){
        orderType = newOrderType;
    }

    //Person: student, no student
    //Premium: not premium, premium
    //second ticket: difference in student or no student
    //10%discount 6 || 6> tickets for non students
    public double calculatePrice(){
        return orderType.calculate(this.tickets);
    };


    public void export(Export exportType) throws IOException {
        exportType.exportTickets(this.tickets,this);
    }
}
