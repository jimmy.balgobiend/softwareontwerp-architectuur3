package StrategyPattern;

import java.util.ArrayList;

public interface OrderType {
    boolean isStudentOrder();
    boolean isWeekend();
    double calculate(ArrayList<MovieTicket> tickets);
}

class StudentWeekDaysOrderType implements OrderType {

    @Override
    public boolean isStudentOrder() {
        return true;
    }

    @Override
    public boolean isWeekend() {
        return false;
    }

    @Override
    public double calculate(ArrayList<MovieTicket> tickets) {
        var totalPrice = 0.00;

        for (int i = 0; i < tickets.size(); i++) {
            if (i % 2 == 0) {
                totalPrice += tickets.get(i).getPrice();
            } else {
                totalPrice += 0;
            }
            if (tickets.get(i).isPremiumTicket()) {
                totalPrice += tickets.get(i).getPrice() + 2;
            }
        }
        if (tickets.size() >= 6) {
            totalPrice = totalPrice * 0.9;
        }
        return totalPrice;
    }
}

class StudentWeekendDaysOrderType implements OrderType {

    @Override
    public boolean isStudentOrder() {
        return true;
    }

    @Override
    public boolean isWeekend() {
        return true;
    }

    @Override
    public double calculate(ArrayList<MovieTicket> tickets) {
        var totalPrice = 0.00;

        for (int i = 0; i < tickets.size(); i++) {
            if (i % 2 == 0) {
                totalPrice += tickets.get(i).getPrice();
            } else {
                totalPrice += 0;
            }
            if (tickets.get(i).isPremiumTicket()) {
                totalPrice += tickets.get(i).getPrice() + 2;
            }
        }
        if (tickets.size() >= 6) {
            totalPrice = totalPrice * 0.9;
        }
        return totalPrice;
    }
}

class NonStudentWeekDaysOrderType implements OrderType {

    @Override
    public boolean isStudentOrder() {
        return false;
    }

    @Override
    public boolean isWeekend() {
        return false;
    }

    @Override
    public double calculate(ArrayList<MovieTicket> tickets) {
        var totalPrice = 0.00;

        for (int i = 0; i < tickets.size(); i++) {
            var dateTicket = tickets.get(i).getMovieScreening().getDateAndTime();
            if (i % 2 == 0) {
                totalPrice += tickets.get(i).getPrice();
            } else {
                totalPrice += 0;
            }
            if (tickets.get(i).isPremiumTicket()) {
                totalPrice += tickets.get(i).getPrice() + 3;
            }
        }
        if (tickets.size() >= 6) {
            totalPrice = totalPrice * 0.9;
        }
        return totalPrice;
    }
}

class NonStudentWeekendEndDaysOrderType implements OrderType {

    @Override
    public boolean isStudentOrder() {
        return false;
    }

    @Override
    public boolean isWeekend() {
        return true;
    }

    @Override
    public double calculate(ArrayList<MovieTicket> tickets) {
        var totalPrice = 0.00;

        for (int i = 0; i < tickets.size(); i++) {
            var dateTicket = tickets.get(i).getMovieScreening().getDateAndTime();
            if (i % 2 == 0) {
                totalPrice += tickets.get(i).getPrice();
            } else {
                totalPrice = totalPrice + tickets.get(i).getPrice();
            }
            if (tickets.get(i).isPremiumTicket()) {
                totalPrice += tickets.get(i).getPrice() + 3;
            }
        }
        if (tickets.size() >= 6) {
            totalPrice = totalPrice * 0.9;
        }
        return totalPrice;
    }
}
