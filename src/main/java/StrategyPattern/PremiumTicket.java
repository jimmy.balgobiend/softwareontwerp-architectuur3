package StrategyPattern;

public class PremiumTicket extends MovieTicket {
    public PremiumTicket(MovieScreening movieScreening, int seatRow, int seatNr) {
        super(movieScreening, seatRow, seatNr);
        ticketType = new isPremium();
    }
}
