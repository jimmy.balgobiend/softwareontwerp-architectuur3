package StrategyPattern;

public class StudentWeekDays extends Order {
    public StudentWeekDays(int orderNr) {
        super(orderNr);
        orderType = new StudentWeekDaysOrderType();
    }
}
