package StrategyPattern;

public class StudentWeekendDays extends Order {
    public StudentWeekendDays(int orderNr) {
        super(orderNr);
        orderType = new StudentWeekendDaysOrderType();
    }
}
