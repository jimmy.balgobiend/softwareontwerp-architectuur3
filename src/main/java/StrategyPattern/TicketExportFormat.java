package StrategyPattern;

public enum TicketExportFormat {
    PLAINTEXT,
    JSON
}
