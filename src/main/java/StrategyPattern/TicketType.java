package StrategyPattern;

public interface TicketType {
    boolean isPremiumTicket();
}

class isPremium implements TicketType {

    @Override
    public boolean isPremiumTicket() {
        return true;
    }
}

class isNotPremium implements TicketType {

    @Override
    public boolean isPremiumTicket() {
        return false;
    }
}
