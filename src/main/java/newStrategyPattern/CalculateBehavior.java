package newStrategyPattern;


import java.util.ArrayList;

public interface CalculateBehavior {
    double calculate(ArrayList<MovieTicket> tickets);
}
