package newStrategyPattern;

import java.time.DayOfWeek;
import java.util.ArrayList;

public class CalculateForNonStudents implements CalculateBehavior {

    @Override
    public double calculate(ArrayList<MovieTicket> tickets) {
        var totalPrice = 0.00;

        for (int i = 0; i < tickets.size(); i++) {
            var dateTicket = tickets.get(i).getMovieScreening().getDateAndTime();
            //is student
                //non students
                //second ticket is free for everyone on ma/di/wo/do
            if (i % 2 == 0) {
                totalPrice += tickets.get(i).getPrice();
            } else {
                if (dateTicket.getDayOfWeek() == DayOfWeek.MONDAY || dateTicket.getDayOfWeek() == DayOfWeek.TUESDAY || dateTicket.getDayOfWeek() == DayOfWeek.WEDNESDAY || dateTicket.getDayOfWeek() == DayOfWeek.THURSDAY) {
                    totalPrice += 0;
                } else {
                    //normal price if on FRI/SAT/SUN
                    totalPrice = totalPrice + tickets.get(i).getPrice();
                }
            }
            //premium tickets for non students = 3$ more expensive then standard price per seat.
            if (tickets.get(i).isPremiumTicket()) {
                totalPrice += tickets.get(i).getPrice() + 3;
            }
        }
        //weekend as non student = fill price, unless order = 6 tickets or 6> then 10% discount.
        if (tickets.size() >= 6) {
            totalPrice = totalPrice * 0.9;
        }
        return totalPrice;
    }
}
