package newStrategyPattern;

import java.time.DayOfWeek;
import java.util.ArrayList;

public class CalculateForStudents  implements CalculateBehavior {
    @Override
    public double calculate(ArrayList<MovieTicket> tickets) {
        var totalPrice = 0.00;

        for (int i = 0; i < tickets.size(); i++) {
            var dateTicket = tickets.get(i).getMovieScreening().getDateAndTime();
            //is student
                //second ticket is free for students(every day)
                if (i % 2 == 0) {
                    totalPrice += tickets.get(i).getPrice();
                } else {
                    totalPrice += 0;
                }
                //premium tickets for students = 2$ more expensive then standard price per seat.
                if (tickets.get(i).isPremiumTicket()) {
                    totalPrice += tickets.get(i).getPrice() + 2;
                }

        }
        //weekend as non student = fill price, unless order = 6 tickets or 6> then 10% discount.
        if (tickets.size() >= 6) {
            totalPrice = totalPrice * 0.9;
        }
        return totalPrice;
    }
}
