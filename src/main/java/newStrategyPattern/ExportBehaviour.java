package newStrategyPattern;

import java.util.ArrayList;

public interface ExportBehaviour {
    void export(ArrayList<MovieTicket> tickets,Order order) throws Exception;
}
