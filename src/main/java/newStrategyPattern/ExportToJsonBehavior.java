package newStrategyPattern;



import java.io.FileWriter;
import java.util.ArrayList;

public class ExportToJsonBehavior implements ExportBehaviour{

    @Override
    public void export(ArrayList<MovieTicket> tickets, Order order) throws Exception {
        try( FileWriter writer = new FileWriter("testfile.json")){
            for (int i = 0; i < tickets.size(); i++) {
                writer.write(tickets.get(i).toString());
                writer.write(tickets.get(i).getMovieScreening().toString());
                writer.write(String.valueOf(order.calculatePrice()));
            }
        }catch(Exception e){
            System.err.println(e);
        }
    }
}
