package newStrategyPattern;

public class JsonExport extends Order {

    public JsonExport(int orderNr,boolean isStudentOrder) {
        super(orderNr, isStudentOrder);
        exportBehavior = new ExportToJsonBehavior();
    }
}
