package newStrategyPattern;

import StrategyPattern.ExportToJSON;
import StrategyPattern.ExportToPlainText;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        // write your code here
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();

        // Create movies
        movies.add(new Movie("Avengers"));
        movies.add(new Movie("Spiderman"));

        // Create screenings
        movies.get(0).addScreening(new MovieScreening(movies.get(0), LocalDateTime.now(), 11.50));
        movies.get(1).addScreening(new MovieScreening(movies.get(1), LocalDateTime.now().plusDays(2), 14.50));

        // Create orders
        // 1st
        orders.add(new NonStudentOrder(1));
        orders.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 155));
        orders.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 156));
        System.out.println(orders.get(0).calculatePrice());
        // 2e
        orders.add(new StudentOrder(2));
        orders.get(1).addSeatReservation(new MovieTicket(movies.get(1).getMovieScreenings().get(0), true, 12, 75));
        // 3e
        orders.add(new StudentOrder(3));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 100));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 101));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 102));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 103));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 104));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 105));
        orders.get(2).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 21, 106));

        List<Movie> movies1 = new ArrayList<>();
        movies1.add(new Movie("Avengers"));
        List<Order> orders1 = new ArrayList<>();
        orders1.add(new StudentOrder(1));
        orders1.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 155));
        orders1.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 156));
        orders1.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 155));
        orders1.get(0).addSeatReservation(new MovieTicket(movies.get(0).getMovieScreenings().get(0), false, 23, 156));

        orders1.get(0).calculatePrice();

        for(int i =0; i < orders.size(); i++){
            System.out.println(orders.get(i).toString());
        }

        System.out.println(orders.get(2).calculatePrice());

        // Results
        orders.get(0).export(new ExportToJsonBehavior());
        orders.get(2).export(new ExportToTextBehavior());


    }
}
