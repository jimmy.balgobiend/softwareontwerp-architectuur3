package newStrategyPattern;
public class MovieTicket {
    private int rowNr;
    private int seatNr;
    private boolean isPremium;
    private MovieScreening movieScreening;

    public MovieTicket(MovieScreening movieScreening,boolean isPremiumReservation,int seatRow, int seatNr) {
        this.rowNr = seatRow;
        this.seatNr = seatNr;
        this.isPremium = isPremiumReservation;
        this.movieScreening = movieScreening;
    }

    public MovieScreening getMovieScreening() {
        return movieScreening;
    }

    public boolean isPremiumTicket(){
        return this.isPremium;
    }

    public double getPrice(){
        return this.movieScreening.getPricePerSeat();
    }

    @Override
    public String toString() {
        return "MovieTicket{" +
                "rowNr=" + rowNr +
                ", seatNr=" + seatNr +
                ", isPremium=" + isPremium +
                ", movieScreening=" + movieScreening +
                '}';
    }
}
