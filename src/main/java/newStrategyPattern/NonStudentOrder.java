package newStrategyPattern;

public class NonStudentOrder extends Order {
    public NonStudentOrder(int orderNr) {
        super(orderNr, false);
        calculateBehavior = new CalculateForNonStudents();
    }
}
