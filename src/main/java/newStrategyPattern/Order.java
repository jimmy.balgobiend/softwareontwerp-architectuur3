package newStrategyPattern;
import StrategyPattern.Export;

import java.io.FileWriter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

public abstract class Order {
    private final int orderNr;
    private final boolean isStudentOrder;
    private final ArrayList<MovieTicket> tickets = new ArrayList<>();
    public CalculateBehavior calculateBehavior;
    public ExportBehaviour exportBehavior;




    public Order(int orderNr, boolean isStudentOrder) {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;
    }

    public void setExportBehavior(ExportBehaviour exportBehavior) {
        this.exportBehavior = exportBehavior;
    }

    public boolean isStudentOrder() {
        return isStudentOrder;
    }

    public List<MovieTicket> getTickets() {
        return tickets;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket) {
        this.tickets.add(ticket);
    }


    //Person: student, no student
    //Premium: not premium, premium
    //second ticket: difference in student or no student
    //10%discount 6 || 6> tickets for non students
    public double calculatePrice() {
        return calculateBehavior.calculate(this.tickets);
    }


    public void setCalculateBehavior(CalculateBehavior calculateBehavior) {
        this.calculateBehavior = calculateBehavior;
    }

    public void export(ExportBehaviour exportBehavior) throws Exception {
        exportBehavior.export(this.tickets,this);
    }
}
