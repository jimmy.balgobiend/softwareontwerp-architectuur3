package newStrategyPattern;

public class StudentOrder extends Order {
    public StudentOrder(int orderNr) {
        super(orderNr, true);
        calculateBehavior = new CalculateForStudents();
    }
}
