package newStrategyPattern;

public class TextExport extends Order{
    public TextExport(int orderNr, boolean isStudentOrder) {
        super(orderNr, isStudentOrder);
        exportBehavior = new ExportToTextBehavior();
    }
}
