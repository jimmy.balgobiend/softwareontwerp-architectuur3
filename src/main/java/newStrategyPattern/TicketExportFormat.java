package newStrategyPattern;
public enum TicketExportFormat {
    PLAINTEXT,
    JSON
}
