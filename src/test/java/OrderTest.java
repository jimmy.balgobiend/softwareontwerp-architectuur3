
import java.io.IOException;
import java.time.DayOfWeek;

import StrategyPattern.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void test(){
        Order order = new NonStudentWeekDays(1);
        assertEquals(1, order.getOrderNr());
    }

    @Test
    void OneTicketNoStudentMondayTenAvengersNoPremium(){
        //basic calculate test
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        movies.add(new Movie("Avengers"));

        LocalDateTime previousMonday =
                LocalDateTime.now( ZoneId.of( "America/Montreal" ) )
                        .with( TemporalAdjusters.previous( DayOfWeek.MONDAY ));

        orders.add(new NonStudentWeekDays(1));
        movies.get(0).addScreening(new MovieScreening(movies.get(0), previousMonday, 10.00));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0),  23, 155));

        assertEquals(1, orders.size());
        assertEquals(1, orders.get(0).getTickets().size());
        assertEquals(false, orders.get(0).isStudentOrder());

        List<MovieTicket> tickets = orders.get(0).getTickets();

        var dateTicket = tickets.get(0).getMovieScreening().getDateAndTime();
        assertEquals(DayOfWeek.MONDAY,dateTicket.getDayOfWeek());

        var price = 0;
        for (var ticket : tickets)
        {
            price += ticket.getPrice();
        }
        assertEquals(10.00, price);
        assertEquals(10.00, tickets.get(0).getMovieScreening().getPricePerSeat());
        assertEquals("Avengers",movies.get(0).getTitle());
        assertEquals(false, tickets.get(0).isPremiumTicket());
        assertEquals(10.00, orders.get(0).calculatePrice());
    }

    @Test
    void TwoTicketNoStudentMondayTenAvengersNoPremium(){
        //basic calculate test
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        movies.add(new Movie("Avengers"));

        LocalDateTime previousMonday =
                LocalDateTime.now( ZoneId.of( "America/Montreal" ) )
                        .with( TemporalAdjusters.previous( DayOfWeek.MONDAY ));

        orders.add(new NonStudentWeekDays(1));
        movies.get(0).addScreening(new MovieScreening(movies.get(0), previousMonday, 10.00));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));

        assertEquals(1, orders.size());
        assertEquals(2, orders.get(0).getTickets().size());
        assertEquals(false, orders.get(0).isStudentOrder());

        List<MovieTicket> tickets = orders.get(0).getTickets();

        var dateTicket = tickets.get(0).getMovieScreening().getDateAndTime();
        assertEquals(DayOfWeek.MONDAY,dateTicket.getDayOfWeek());

        var price = 0;
        for (var ticket : tickets)
        {
            price += ticket.getPrice();
        }
        assertEquals(20.00, price);
        assertEquals(10.00, tickets.get(0).getMovieScreening().getPricePerSeat());
        assertEquals("Avengers",movies.get(0).getTitle());
        assertEquals(false, tickets.get(0).isPremiumTicket());
        assertEquals(10.00, orders.get(0).calculatePrice());
    }
    @Test
    void TwoTicketNoStudentSaturdayTenAvengersPremium(){
        //basic calculate test
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        movies.add(new Movie("Avengers"));

        LocalDateTime previousSaturday =
                LocalDateTime.now( ZoneId.of( "America/Montreal" ) )
                        .with( TemporalAdjusters.previous( DayOfWeek.SATURDAY ));

        orders.add(new NonStudentWeekendDays(1));
        movies.get(0).addScreening(new MovieScreening(movies.get(0), previousSaturday, 10.00));
        orders.get(0).addSeatReservation(new PremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));
        orders.get(0).addSeatReservation(new PremiumTicket(movies.get(0).getMovieScreenings().get(0),  23, 155));

        assertEquals(1, orders.size());
        assertEquals(2, orders.get(0).getTickets().size());
        assertEquals(false, orders.get(0).isStudentOrder());

        List<MovieTicket> tickets = orders.get(0).getTickets();

        var dateTicket = tickets.get(0).getMovieScreening().getDateAndTime();
        assertEquals(DayOfWeek.SATURDAY,dateTicket.getDayOfWeek());

        var price = 0;
        for (var ticket : tickets)
        {
            price += ticket.getPrice();
        }
        assertEquals(20.00, price);
        assertEquals(10.00, tickets.get(0).getMovieScreening().getPricePerSeat());
        assertEquals("Avengers",movies.get(0).getTitle());
        assertEquals(true, tickets.get(0).isPremiumTicket());
        assertEquals(46.00, orders.get(0).calculatePrice());
    }
    @Test
    void OneTicketStudentMondayTenAvengersPremium(){
        //basic calculate test
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        movies.add(new Movie("Avengers"));

        LocalDateTime previousMonday =
                LocalDateTime.now( ZoneId.of( "America/Montreal" ) )
                        .with( TemporalAdjusters.previous( DayOfWeek.MONDAY ));

        orders.add(new StudentWeekDays(1));
        movies.get(0).addScreening(new MovieScreening(movies.get(0), previousMonday, 10.00));
        orders.get(0).addSeatReservation(new PremiumTicket(movies.get(0).getMovieScreenings().get(0),  23, 155));

        assertEquals(1, orders.size());
        assertEquals(1, orders.get(0).getTickets().size());
        assertEquals(true, orders.get(0).isStudentOrder());

        List<MovieTicket> tickets = orders.get(0).getTickets();

        var dateTicket = tickets.get(0).getMovieScreening().getDateAndTime();
        assertEquals(DayOfWeek.MONDAY,dateTicket.getDayOfWeek());

        var price = 0;
        for (var ticket : tickets)
        {
            price += ticket.getPrice();
        }
        assertEquals(10.00, price);
        assertEquals(10.00, tickets.get(0).getMovieScreening().getPricePerSeat());
        assertEquals("Avengers",movies.get(0).getTitle());
        assertEquals(true, tickets.get(0).isPremiumTicket());
        assertEquals(22.00, orders.get(0).calculatePrice());
    }
    @Test
    void TwoTicketStudentMondayTenAvengersNoPremium(){
        //basic calculate test
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        movies.add(new Movie("Avengers"));

        LocalDateTime previousMonday =
                LocalDateTime.now( ZoneId.of( "America/Montreal" ) )
                        .with( TemporalAdjusters.previous( DayOfWeek.MONDAY ));

        orders.add(new StudentWeekDays(1));
        movies.get(0).addScreening(new MovieScreening(movies.get(0), previousMonday, 10.00));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));

        assertEquals(1, orders.size());
        assertEquals(2, orders.get(0).getTickets().size());
        assertEquals(true, orders.get(0).isStudentOrder());

        List<MovieTicket> tickets = orders.get(0).getTickets();
        var dateTicket = tickets.get(0).getMovieScreening().getDateAndTime();

        var price = 0;
        for (var ticket : tickets)
        {
            price += ticket.getPrice();
        }
        assertEquals(20.00, price);
        assertEquals(DayOfWeek.MONDAY,dateTicket.getDayOfWeek());
        assertEquals(10.00, tickets.get(0).getMovieScreening().getPricePerSeat());
        assertEquals("Avengers",movies.get(0).getTitle());
        assertEquals(false, tickets.get(0).isPremiumTicket());
        assertEquals(10.00, orders.get(0).calculatePrice());
    }
    @Test
    void SixTicketStudentMondayTenAvengersNoPremium(){
        //basic calculate test
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        movies.add(new Movie("Avengers"));

        LocalDateTime previousMonday =
                LocalDateTime.now( ZoneId.of( "America/Montreal" ) )
                        .with( TemporalAdjusters.previous( DayOfWeek.MONDAY ));

        orders.add(new StudentWeekDays(1));
        movies.get(0).addScreening(new MovieScreening(movies.get(0), previousMonday, 10.00));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0),  23, 155));;
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));;
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));;
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));;
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));

        assertEquals(1, orders.size());
        assertEquals(6, orders.get(0).getTickets().size());
        assertEquals(true, orders.get(0).isStudentOrder());

        List<MovieTicket> tickets = orders.get(0).getTickets();
        var dateTicket = tickets.get(0).getMovieScreening().getDateAndTime();
        assertEquals(DayOfWeek.MONDAY,dateTicket.getDayOfWeek());

        var price = 0;
        for (var ticket : tickets)
        {
            price += ticket.getPrice();
        }
        assertEquals(60.00, price);
        assertEquals(10.00, tickets.get(0).getMovieScreening().getPricePerSeat());
        assertEquals("Avengers",movies.get(0).getTitle());
        assertEquals(false, tickets.get(0).isPremiumTicket());
        assertEquals(27.00, orders.get(0).calculatePrice());
    }

    @Test
    void WriteToTextFile() throws IOException {
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        // Create movies
        movies.add(new Movie("Avengers"));
        movies.add(new Movie("Spiderman"));

        // Create screenings
        movies.get(0).addScreening(new MovieScreening(movies.get(0), LocalDateTime.now(), 11.50));
        movies.get(1).addScreening(new MovieScreening(movies.get(1), LocalDateTime.now().plusDays(2), 14.50));

        // Create orders
        // 1st
        orders.add(new NonStudentWeekDays(1));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));

        orders.get(0).export(new ExportToPlainText());

        assertEquals(1, 1);
    }

    @Test
    void WriteToJsonFile() throws IOException {
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        // Create movies
        movies.add(new Movie("Avengers"));
        movies.add(new Movie("Spiderman"));

        // Create screenings
        movies.get(0).addScreening(new MovieScreening(movies.get(0), LocalDateTime.now(), 11.50));
        movies.get(1).addScreening(new MovieScreening(movies.get(1), LocalDateTime.now().plusDays(2), 14.50));

        // Create orders
        // 1st
        orders.add(new NonStudentWeekDays(1));
        orders.get(0).addSeatReservation(new NonPremiumTicket(movies.get(0).getMovieScreenings().get(0), 23, 155));

        orders.get(0).export(new ExportToJSON());

        assertEquals(1, 1);
    }
}